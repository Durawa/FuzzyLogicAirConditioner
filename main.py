import numpy as np
import matplotlib.pyplot as plt


def convertDiffToIdx(diff : float):
    return (diff - (-4)) * 20




tempErrorArray = np.zeros([8, 161])

for i in range(-80, 81):
    err = i/20
    idx = i + 80
    tempErrorArray[0, idx] = err

    if -4 <= err <= -1.5:
        negLargeVal = 1
    elif -1.5 < err <= -1:
        negLargeVal = (-1 - err) / (-1 - (-1.5))
    else:
        negLargeVal = 0
    tempErrorArray[1, idx] = negLargeVal


    if -1.5 <= err <= -1:
        negMediumVal = (err - (-1.5)) / (-1 - (-1.5))
    elif -1 < err <= -0.5:
        negMediumVal = (-0.5 - err) / (-0.5 - (-1))
    else:
        negMediumVal = 0
    tempErrorArray[2, idx] = negMediumVal


    if -1 <= err <= -0.5:
        negSmallVal = (err - (-1)) / (-0.5 - (-1))
    elif -0.5 < err <= 0:
        negSmallVal = (0 - err) / (0 - (-0.5))
    else:
        negSmallVal = 0
    tempErrorArray[3, idx] = negSmallVal


    if -0.5 <= err <= 0:
        zeroVal = (err - (-0.5)) / ( 0 - (-0.5))
    elif 0 < err <= 0.5:
        zeroVal = (0.5 - err) / (0.5 - 0)
    else:
        zeroVal = 0
    tempErrorArray[4, idx] = zeroVal


    if 0 <= err <= 0.5:
        posSmallVal = (err - 0) / (0.5 - 0)
    elif 0.5 < err <= 1:
        posSmallVal = (1 - err) / (1 - 0.5)
    else:
        posSmallVal = 0
    tempErrorArray[5, idx] = posSmallVal


    if 0.5 <= err <= 1:
        posMediumVal = (err - 0.5) / (1 - 0.5)
    elif 1 < err <= 1.5:
        posMediumVal = (1.5 - err) / (1.5 - 1)
    else:
        posMediumVal = 0
    tempErrorArray[6, idx] = posMediumVal


    if 1 <= err <= 1.5:
        posLargeVal = (err - 1) / (1.5 - 1)
    elif 1.5 < err:
        posLargeVal = 1
    else:
        posLargeVal = 0
    tempErrorArray[7, idx] = posLargeVal





powerArray = np.zeros([8, 101])

for i in range(0, 101):
    pow = i
    powerArray[0, i] = pow

    if 0 <= pow <= 5:
        off = (5 - pow) / (5 - 0)
    else:
        off = 0
    powerArray[1, i] = off


    if 0 <= pow <= 15:
        veryLow = (pow - 0) / (15 - 0)
    elif 15 < pow <= 30:
        veryLow = (30 - pow) / (30 - 15)
    else:
        veryLow = 0
    powerArray[2, i] = veryLow


    if 25 <= pow <= 35:
        low = (pow - 25) / (35 -25)
    elif 35 < pow <= 45:
        low = (45 - pow) / (45 -35)
    else:
        low = 0
    powerArray[3, i] = low


    if 40 <= pow <= 50:
        medium = (pow - 40) / (50 - 40)
    elif 50 < pow <= 60:
        medium = (60 - pow) / (60 - 50)
    else:
        medium = 0
    powerArray[4, i] = medium


    if 55 <= pow <= 70:
        powerful = (pow - 55) / (70 - 55)
    elif 70 < pow <= 85:
        powerful = (85 - pow) / (85 - 70)
    else:
        powerful = 0
    powerArray[5, i] = powerful


    if 70 <= pow <= 85:
        veryPowerful = (pow - 70) / (85 - 70)
    elif 85 < pow <= 100:
        veryPowerful = (100 - pow) / (100 - 85)
    else:
        veryPowerful = 0
    powerArray[6, i] = veryPowerful

    if 90 <= pow <= 100:
        maximum = (pow - 90) / (100 - 90)
    else:
        maximum = 0
    powerArray[7, i] = maximum

#Rozmywanie
actual = 18
input = float(input("Podaj pozadana temperature: "))
diff = input - actual
index = int(convertDiffToIdx(diff))


uNegLargeVal = tempErrorArray[1, index]
uNegMediumVal = tempErrorArray[2, index]
uNegSmallVal = tempErrorArray[3, index]
uZeroVal = tempErrorArray[4, index]
uPosSmallVal = tempErrorArray[5, index]
uPosMediumVal = tempErrorArray[6, index]
uPosLargeVal = tempErrorArray[7, index]


#Wyostrzanie

resultArray = np.zeros([7, 101])
outputArray = np.zeros([2, 101])
numerator = 0
denominator = 0
result = 0

for i in range(0, 101):

    resultArray[0, i] = min(uZeroVal, powerArray[1, i])
    resultArray[1, i] = min(min(uZeroVal, uPosSmallVal), powerArray[2, i])
    resultArray[2, i] = min(uPosSmallVal, powerArray[3, i])
    resultArray[3, i] = min(min(uPosSmallVal, uPosMediumVal), powerArray[4, i])
    resultArray[4, i] = min(uPosMediumVal, powerArray[5, i])
    resultArray[5, i] = min(min(uPosMediumVal, uPosLargeVal), powerArray[6, i])
    resultArray[6, i] = min(uPosLargeVal, powerArray[7, i])

#Wnioskowanie
    outputArray[0, i] = i
    outputArray[1, i] = max(resultArray[0, i], resultArray[1, i], resultArray[2, i],
                            resultArray[3, i], resultArray[4, i], resultArray[5, i], resultArray[6, i])

    numerator += i * outputArray[1, i]
    denominator += outputArray[1, i]
result = numerator / denominator
print("Wynik: "+ str(result))




"""
plt.plot(tempErrorArray[0, :], tempErrorArray[1, :], 'b')
plt.plot(tempErrorArray[0, :], tempErrorArray[2, :], 'm')
plt.plot(tempErrorArray[0, :], tempErrorArray[3, :], 'g')
plt.plot(tempErrorArray[0, :], tempErrorArray[4, :], 'k')
plt.plot(tempErrorArray[0, :], tempErrorArray[5, :], 'y')
plt.plot(tempErrorArray[0, :], tempErrorArray[6, :], 'c')
plt.plot(tempErrorArray[0, :], tempErrorArray[7, :], 'r')


plt.plot(powerArray[0, :], powerArray[1, :], 'c')
plt.plot(powerArray[0, :], powerArray[2, :], 'm')
plt.plot(powerArray[0, :], powerArray[3, :], 'b')
plt.plot(powerArray[0, :], powerArray[4, :], 'r')
plt.plot(powerArray[0, :], powerArray[5, :], 'g')
plt.plot(powerArray[0, :], powerArray[6, :], 'y')
plt.plot(powerArray[0, :], powerArray[7, :], 'k')
"""


#lt.axis([-4.2, 4.2, 0, 1.2])


#plt.xlabel('')
#plt.ylabel('u()')

plt.plot(outputArray[0, :], outputArray[1, :], 'm')
plt.axis([0, 100, 0, 1.2])
plt.show()



